package presentation.home

import androidx.compose.animation.core.*
import androidx.compose.foundation.Image
import androidx.compose.foundation.layout.*
import androidx.compose.foundation.rememberScrollState
import androidx.compose.foundation.shape.CircleShape
import androidx.compose.foundation.verticalScroll
import androidx.compose.material3.MaterialTheme
import androidx.compose.material3.Surface
import androidx.compose.material3.Text
import androidx.compose.runtime.Composable
import androidx.compose.runtime.getValue
import androidx.compose.runtime.mutableStateOf
import androidx.compose.runtime.remember
import androidx.compose.ui.Alignment
import androidx.compose.ui.Modifier
import androidx.compose.ui.draw.clip
import androidx.compose.ui.draw.drawBehind
import androidx.compose.ui.graphics.Brush
import androidx.compose.ui.graphics.TileMode
import androidx.compose.ui.graphics.drawscope.Stroke
import androidx.compose.ui.graphics.drawscope.rotate
import androidx.compose.ui.graphics.graphicsLayer
import androidx.compose.ui.unit.dp
import org.jetbrains.compose.resources.ExperimentalResourceApi
import org.jetbrains.compose.resources.painterResource
import presentation.theme.Default
import presentation.theme.DoubleDefault
import presentation.theme.primaryColors

@OptIn(ExperimentalResourceApi::class)
@Composable
fun HomeScreen(modifier: Modifier = Modifier) {
    val infiniteTransition = rememberInfiniteTransition(label = "Picture infinite transition")

    val imageScale by infiniteTransition.animateFloat(
        initialValue = 1f,
        targetValue = 0.9f,
        animationSpec = infiniteRepeatable(
            animation = tween(3000, easing = LinearEasing), repeatMode = RepeatMode.Reverse
        ), label = ""
    )


    val rotationValue by infiniteTransition.animateFloat(
        initialValue = 0f,
        targetValue = 360f,
        animationSpec = infiniteRepeatable(tween(1000, easing = LinearEasing)), label = ""
    )

    Surface(modifier = modifier.fillMaxSize()) {
        Column(
            horizontalAlignment = Alignment.CenterHorizontally,
            modifier = Modifier.verticalScroll(rememberScrollState())
        ) {
            Text(
                text = "Senior Android Developer",
                style = MaterialTheme.typography.titleMedium,
                modifier = Modifier
                    .padding(top = Default)
            )
            val colors = primaryColors
            val gradientBrush by remember {
                mutableStateOf(
                    Brush.horizontalGradient(
                        colors = colors,
                        startX = -10.0f,
                        endX = 400.0f,
                        tileMode = TileMode.Repeated
                    )
                )
            }
            Image(
                painter = painterResource("hussein_al_zuhile.jpg"),
                contentDescription = "My Image",
                Modifier
                    .padding(DoubleDefault)
                    .graphicsLayer {
                        scaleX = imageScale
                        scaleY = imageScale
                    }
                    .drawBehind {
                        rotate(rotationValue) {
                            drawCircle(gradientBrush, style = Stroke(width = 18f))
                        }
                    }
                    .fillMaxWidth()
                    .aspectRatio(1f)
                    .clip(CircleShape),
            )
//            Spacer(modifier = Modifier.padding(vertical = HalfDefault))
//            TitleWithBody(
//                title = stringResource(id = R.string.about_me),
//                body = stringResource(id = R.string.about_me_body)
//            )
//            Spacer(modifier = Modifier.height(Default))
//            TitleWithBody(
//                title = stringResource(id = R.string.education),
//                body = stringResource(id = R.string.education_body)
//            )
//            Spacer(modifier = Modifier.height(Default))
//            TitleWithBody(
//                title = stringResource(id = R.string.technical_expertise),
//                body = stringResource(id = R.string.technical_expertise_body)
//            )
//            Spacer(modifier = Modifier.height(Default))
//            TitleWithBody(
//                title = stringResource(id = R.string.tools_and_platforms),
//                body = stringResource(id = R.string.tools_and_platforms_body)
//            )
//            Spacer(modifier = Modifier.height(Default))
//            TitleWithBody(
//                title = stringResource(id = R.string.specializations),
//                body = stringResource(id = R.string.specializations_body)
//            )
//            Spacer(modifier = Modifier.height(Default))
//            TitleWithBody(
//                title = stringResource(id = R.string.passion_and_dedications),
//                body = stringResource(id = R.string.passion_and_dedications_body)
//            )

        }
    }
}