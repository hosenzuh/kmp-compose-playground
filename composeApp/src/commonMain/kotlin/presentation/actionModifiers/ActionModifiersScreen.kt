package presentation.actionModifiers

//import com.hussein.composeplayground.ui.theme.ComposePlaygroundTheme
//import com.hussein.composeplayground.ui.theme.LightAndNightPreview
//import com.hussein.composeplayground.util.IntentManager.viewUrl
import androidx.compose.foundation.*
import androidx.compose.foundation.gestures.Orientation
import androidx.compose.foundation.gestures.draggable
import androidx.compose.foundation.gestures.rememberDraggableState
import androidx.compose.foundation.interaction.MutableInteractionSource
import androidx.compose.foundation.layout.*
import androidx.compose.foundation.selection.selectable
import androidx.compose.foundation.selection.selectableGroup
import androidx.compose.foundation.selection.toggleable
import androidx.compose.foundation.selection.triStateToggleable
import androidx.compose.foundation.text.ClickableText
import androidx.compose.material.icons.Icons
import androidx.compose.material.icons.filled.*
import androidx.compose.material.icons.outlined.House
import androidx.compose.material.ripple.rememberRipple
import androidx.compose.material3.*
import androidx.compose.runtime.*
import androidx.compose.ui.Alignment
import androidx.compose.ui.Modifier
import androidx.compose.ui.geometry.Offset
import androidx.compose.ui.graphics.Color
import androidx.compose.ui.layout.onGloballyPositioned
import androidx.compose.ui.platform.LocalDensity
import androidx.compose.ui.state.ToggleableState.*
import androidx.compose.ui.text.*
import androidx.compose.ui.text.font.FontStyle
import androidx.compose.ui.text.font.FontWeight
import androidx.compose.ui.text.style.TextDecoration
import androidx.compose.ui.unit.IntOffset
import androidx.compose.ui.unit.dp
import kotlinx.coroutines.launch
import presentation.base.components.ModifierExample
import presentation.base.components.NoteCard
import presentation.theme.Default
import kotlin.math.roundToInt


@OptIn(ExperimentalTextApi::class)
@Composable
fun ActionModifiersScreen(modifier: Modifier = Modifier, onShowCodeSnippetClick: (String) -> Unit) {

    val coroutineScope = rememberCoroutineScope()
    val snackbarHostState = remember {
        SnackbarHostState()
    }
    Box(modifier.fillMaxHeight()) {
        Column(Modifier.verticalScroll(rememberScrollState())) {
            ModifierExample(
                title = "Clickable", body = "Adds on click action",
                codeSnippetImage = "modifier_clickable.png",
                onShowCodeSnippetClick = onShowCodeSnippetClick,
                documentationLink = "https://developer.android.com/reference/kotlin/androidx/compose/ui/Modifier#(androidx.compose.ui.Modifier).clickable(kotlin.Boolean,kotlin.String,androidx.compose.ui.semantics.Role,kotlin.Function0)",
            ) {
                Text("This text now is Clickable", modifier = Modifier
                    .clickable(
                        indication = rememberRipple(),
                        interactionSource = remember { MutableInteractionSource() }) {
                        coroutineScope.launch {
                            snackbarHostState.showSnackbar(
                                "Text Clicked with one tap",
                            )
                        }
                    }
                )
            }
            ModifierExample(
                title = "Combined Clickable",
                body = "This modifier allows you to set listeners to a click, double click and long click actions",
                codeSnippetImage = "modifier_combined_clickable.png",
                onShowCodeSnippetClick = onShowCodeSnippetClick,
                documentationLink = "https://developer.android.com/reference/kotlin/androidx/compose/ui/Modifier#(androidx.compose.ui.Modifier).combinedClickable(kotlin.Boolean,kotlin.String,androidx.compose.ui.semantics.Role,kotlin.String,kotlin.Function0,kotlin.Function0,kotlin.Function0)"
            ) {
                @OptIn(ExperimentalFoundationApi::class)
                Text(
                    "This text now can handle: normal click, long click, double click",
                    modifier = Modifier
                        .combinedClickable(
                            onLongClick = {
                                coroutineScope.launch {
                                    snackbarHostState.showSnackbar(
                                        "Text clicked with long tap",
                                    )
                                }
                            },
                            onDoubleClick = {
                                coroutineScope.launch {
                                    snackbarHostState.showSnackbar(
                                        "Text clicked with double tap",
                                    )
                                }
                            },
                            onClick = {
                                coroutineScope.launch {
                                    snackbarHostState.showSnackbar(
                                        "Text clicked with one tap",
                                    )
                                }
                            })
                )
            }
            ModifierExample(
                title = AnnotatedString("Selectable and Selectable Group").toString(),
                body = buildAnnotatedString {
                    append("The ")
                    withStyle(
                        MaterialTheme.typography.bodyMedium.copy(fontWeight = FontWeight.Bold)
                            .toSpanStyle()
                    ) {
                        append("selectableGroup ")
                    }
                    append("modifier is just an accessibility purpose modifier that group a list of ")
                    withStyle(
                        MaterialTheme.typography.bodyMedium.copy(fontWeight = FontWeight.Bold)
                            .toSpanStyle()
                    ) {
                        append("selectable ")
                    }
                    append("modifier, and Selectable modifier configures a component to be selectable.")
                    append("\n" + "Usually these modifiers used where only one item can be selected at any point, if ")
                }.toString(),
                codeSnippetImage = "modifier_selectable.png",
                onShowCodeSnippetClick = onShowCodeSnippetClick,
                documentationLink = "https://developer.android.com/reference/kotlin/androidx/compose/ui/Modifier#(androidx.compose.ui.Modifier).selectable(kotlin.Boolean,kotlin.Boolean,androidx.compose.ui.semantics.Role,kotlin.Function0)"
            ) {
                var currentSelectedText by remember {
                    mutableIntStateOf(-1)
                }
                Column(modifier = Modifier.selectableGroup()) {
                    Text(
                        text = "This text is selectable ${if (currentSelectedText == 1) "✅" else ""}",
                        Modifier.selectable(selected = currentSelectedText == 1) {
                            currentSelectedText = if (currentSelectedText == 1) -1 else 1
                        }
                    )
                    Text(
                        text = "And this one is selectable too! ${if (currentSelectedText == 2) "✅" else ""}",
                        Modifier
                            .selectable(
                                indication = rememberRipple(),
                                interactionSource = remember {
                                    MutableInteractionSource()
                                },
                                selected = currentSelectedText == 2,
                                onClick = {
                                    currentSelectedText = if (currentSelectedText == 2) -1 else 2
                                })
                    )
                }
            }
            ModifierExample(
                title = AnnotatedString("Toggleable"),
                body = buildAnnotatedString {
                    append("If you want to make an item support on/off capabilities without being part of a set, consider using ")
                    withStyle(
                        MaterialTheme.typography.bodyMedium.copy(fontWeight = FontWeight.Bold)
                            .toSpanStyle()
                    ) {
                        append("Modifier.toggleable")
                    }
                },
                codeSnippetImage = "modifier_toggleable.png",
                onShowCodeSnippetClick = onShowCodeSnippetClick,
                documentationLink = "https://developer.android.com/reference/kotlin/androidx/compose/ui/Modifier#(androidx.compose.ui.Modifier).toggleable(kotlin.Boolean,kotlin.Boolean,androidx.compose.ui.semantics.Role,kotlin.Function1)"
            ) {
                var isToggled by remember {
                    mutableStateOf(false)
                }
                Row(
                    Modifier
                        .fillMaxWidth()
                        .toggleable(isToggled) {
                            isToggled = it
                        }
                        .padding(Default), verticalAlignment = Alignment.CenterVertically
                ) {
                    Icon(
                        imageVector = if (isToggled) Icons.Filled.House else Icons.Outlined.House,
                        contentDescription = "Toggleable Icon",
                    )
                    Spacer(modifier = Modifier.width(Default))
                    Text(text = "This row is toggleable")
                }
            }
            ModifierExample(
                title = AnnotatedString("Tri State Toggleable"),
                body = buildAnnotatedString {
                    append("Same as Toggleable but with ")
                    withStyle(
                        MaterialTheme.typography.bodyMedium.toSpanStyle()
                            .copy(fontStyle = FontStyle.Italic)
                    ) {
                        append("indeterminate ")
                    }
                    appendLine("state between on/off")

                    append("There is ")
                    withStyle(
                        MaterialTheme.typography.bodyMedium.toSpanStyle()
                            .copy(fontWeight = FontWeight.Bold)
                    ) {
                        append("TriStateCheckbox ")
                    }
                    append("component for this use case, when its nested states not fully On and not fully Off")
                },
                codeSnippetImage = "modifier_tri_state_toggleable.png",
                onShowCodeSnippetClick = onShowCodeSnippetClick,
                documentationLink = "https://developer.android.com/reference/kotlin/androidx/compose/ui/Modifier#(androidx.compose.ui.Modifier).triStateToggleable(androidx.compose.ui.state.ToggleableState,kotlin.Boolean,androidx.compose.ui.semantics.Role,kotlin.Function0)"
            ) {
                var isUiModeLightState by remember {
                    mutableStateOf(Indeterminate)
                }
                Row(
                    Modifier
                        .triStateToggleable(isUiModeLightState) {
                            isUiModeLightState = when (isUiModeLightState) {
                                On -> Off
                                Off -> On
                                Indeterminate -> On
                            }
                        }
                        .padding(Default),
                    verticalAlignment = Alignment.CenterVertically,
                ) {
                    Switch(
                        checked = isUiModeLightState == On,
                        onCheckedChange = {
                            isUiModeLightState = when (isUiModeLightState) {
                                On -> Off
                                Off -> On
                                Indeterminate -> On
                            }
                        },
                        thumbContent = {
                            Icon(
                                imageVector =
                                when (isUiModeLightState) {
                                    On -> Icons.Default.LightMode
                                    Off -> Icons.Default.ModeNight
                                    Indeterminate -> Icons.Default.Brightness6
                                },
                                contentDescription = "UI mode"
                            )
                        }
                    )
                    Spacer(modifier = Modifier.width(Default))
                    Text(
                        text = when (isUiModeLightState) {
                            On -> "Light Mode"
                            Off -> "Night Mode"
                            Indeterminate -> "System default"
                        }
                    )
                    Spacer(modifier = Modifier.weight(1f))
                    IconButton(onClick = { isUiModeLightState = Indeterminate }) {
                        Icon(
                            imageVector = Icons.Default.BrightnessAuto,
                            contentDescription = "Auto Brightness"
                        )
                    }
                }
            }
            NoteCard(Modifier.fillMaxWidth()) {
                val semanticTreeDocumentationUrl =
                    "https://developer.android.com/jetpack/compose/semantics"
                val annotatedString = buildAnnotatedString {
                    append(
                        "The main difference between using selectable with selectable group, toggleable or handling the selecting manually" +
                                " is the "
                    )
                    pushUrlAnnotation(UrlAnnotation(semanticTreeDocumentationUrl))
                    withStyle(
                        MaterialTheme.typography.bodyMedium.toSpanStyle()
                            .copy(Color.Blue, textDecoration = TextDecoration.Underline)
                    ) {
                        append("Semantic Tree")
                    }
                    pop()
                    append(" This helps the accessibility and testing purposes to be very useful")
                }
//                val context = LocalContext.current

                ClickableText(text = annotatedString, onClick = {
                    annotatedString.getUrlAnnotations(it, it)
                        .find { it.item.url == semanticTreeDocumentationUrl }?.let {
//                            context.viewUrl(semanticTreeDocumentationUrl)
                        }
                })
            }
            Divider()
            ModifierExample(
                title = "Draggable",
                body = "This Modifier allows you to apply dragging action on the composable",
                codeSnippetImage = "modifier_draggable.png",
                onShowCodeSnippetClick = onShowCodeSnippetClick,
                documentationLink = "https://developer.android.com/reference/kotlin/androidx/compose/ui/Modifier#(androidx.compose.ui.Modifier).draggable(androidx.compose.foundation.gestures.DraggableState,androidx.compose.foundation.gestures.Orientation,kotlin.Boolean,androidx.compose.foundation.interaction.MutableInteractionSource,kotlin.Boolean,kotlin.coroutines.SuspendFunction2,kotlin.coroutines.SuspendFunction2,kotlin.Boolean)"
            ) {
                var offset by remember {
                    mutableStateOf(Offset(0f, 0f))
                }
                var textWidthInPx by remember {
                    mutableIntStateOf(0)
                }
                val screenWidthInPx =
                    with(LocalDensity.current) {
                        // TODO: Add a function for each platform
                        40.dp.toPx()
//                        LocalConfiguration.current.screenWidthDp.dp.toPx()
                    }
                val horizontalDraggableState = rememberDraggableState(onDelta = {
                    offset =
                        offset.copy(x = (offset.x + it).coerceIn(0f..(screenWidthInPx - textWidthInPx)))
                })
                Text(
                    text = "This text is draggable horizontally",
                    modifier = Modifier
                        .onGloballyPositioned {
                            textWidthInPx = it.size.width
                        }
                        .offset {
                            IntOffset(x = offset.x.roundToInt(), y = 0)
                        }
                        .draggable(
                            horizontalDraggableState,
                            orientation = Orientation.Horizontal,
                            onDragStopped = {
                                // you can add this code to make it back to the initial stat after finish the dragging
                                /*
                                horizontalDraggableState.drag {
                                    animate(it, 0f) { value, _ ->
                                        offset = offset.copy(x = value)
                                    }
                                }
                                */
                            }
                        )
                )
            }
//            @OptIn(ExperimentalFoundationApi::class)
//            ModifierExample(
//                title = "Anchored Draggable",
//                body = "Enable dragging between a set of predefined positions and states.",
//                codeSnippetImage = "modifier_anchored_draggable.png",
//                onShowCodeSnippetClick = onShowCodeSnippetClick,
//                documentationLink = "https://www.google.com/url?sa=t&rct=j&q=&esrc=s&source=web&cd=&cad=rja&uact=8&ved=2ahUKEwidxKHstpaBAxV7k4kEHQixABgQFnoECA8QAQ&url=https%3A%2F%2Ffvilarino.medium.com%2Fexploring-jetpack-compose-anchored-draggable-modifier-5fdb21a0c64c&usg=AOvVaw2Q2GiO5XU0NwN0GWCM8-eN&opi=89978449"
//            ) {
//
//                val density = LocalDensity.current
//
//                val anchoredDraggableState = remember {
//                    AnchoredDraggableState<DragAnchors>(
//                        initialValue = START,
//                        positionalThreshold = { totalDistance: Float ->
//                            totalDistance * 0.5f
//                        },
//                        velocityThreshold = {
//                            with(density) { 100.dp.toPx() }
//                        },
//                        animationSpec = spring(dampingRatio = Spring.DampingRatioLowBouncy),
//                    )
//                }
//
//                var textWidthInPx by remember { mutableIntStateOf(0) }
//                val screenWidthInPx =
//                    with(density) { LocalConfiguration.current.screenWidthDp.dp.toPx() }
//                val dragEndPoint = screenWidthInPx - textWidthInPx
//                LaunchedEffect(key1 = textWidthInPx) {
//                    anchoredDraggableState.updateAnchors(DraggableAnchors {
//                        values()
//                            .forEach { anchor ->
//                                anchor at (dragEndPoint * anchor.fraction)
//                            }
//                    })
//                }
//
//                Text(
//                    text = "This text is draggable\n with anchors",
//                    Modifier
//                        .onGloballyPositioned {
//                            textWidthInPx = it.size.width
//                            anchoredDraggableState.updateAnchors(DraggableAnchors {
//                                values()
//                                    .forEach { anchor ->
//                                        anchor at (dragEndPoint * anchor.fraction)
//                                    }
//                            })
//                        }
//                        .offset {
//                            IntOffset(
//                                x = anchoredDraggableState.offset
//                                    .takeUnless { it.isNaN() }
//                                    ?.roundToInt() ?: 0, y = 0)
//                        }
//                        .anchoredDraggable(
//                            anchoredDraggableState,
//                            Orientation.Horizontal,
//                        )
//                )
//            }
        }
        SnackbarHost(snackbarHostState, modifier = Modifier.align(Alignment.BottomCenter))
    }
}

enum class DragAnchors(val fraction: Float) { START(0f), CENTER(.5f), END(1f) }

//@LightAndNightPreview
//@Composable
//fun ActionModifiersScreenPreview() {
//    ComposePlaygroundTheme {
//        ActionModifiersScreen() {}
//    }
//}