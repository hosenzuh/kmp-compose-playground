package presentation.theme

import androidx.compose.material.MaterialTheme
import androidx.compose.runtime.Composable
import androidx.compose.ui.graphics.Color

val primaryColors: List<Color>
    @Composable get() = listOf(
        MaterialTheme.colors.primary,
        MaterialTheme.colors.primaryVariant,
    )