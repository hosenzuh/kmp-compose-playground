package presentation.base.components

import androidx.compose.animation.animateColor
import androidx.compose.animation.core.*
import androidx.compose.material3.MaterialTheme
import androidx.compose.material3.Text
import androidx.compose.runtime.Composable
import androidx.compose.runtime.getValue
import androidx.compose.ui.Modifier
import androidx.compose.ui.draw.scale
import presentation.theme.primaryColors

@Composable
fun LoadingText(modifier: Modifier = Modifier) {
    val infiniteTransition = rememberInfiniteTransition(label = "")
    val colorAnimation by infiniteTransition.animateColor(
        initialValue = primaryColors.first(),
        targetValue = primaryColors.last(),
        animationSpec = infiniteRepeatable(
            animation = tween(
                2000,
                easing = LinearEasing
            ), RepeatMode.Reverse
        ),
        label = ""
    )
    val scaleAnimation by infiniteTransition.animateFloat(
        initialValue = 1f,
        targetValue = 1.5f,
        animationSpec = infiniteRepeatable(
            animation = tween(
                2000,
                easing = LinearEasing
            ), RepeatMode.Reverse
        ),
        label = ""
    )
    Text(
        text = "Loading",
        modifier = modifier
            .scale(scaleAnimation),
        color = colorAnimation
    )
}

@Composable
fun LoadingTextPreview() {
    MaterialTheme {
        LoadingText()
    }
}