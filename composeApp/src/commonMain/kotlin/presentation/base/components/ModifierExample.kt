package presentation.base.components

import androidx.compose.foundation.layout.*
import androidx.compose.material.icons.Icons
import androidx.compose.material.icons.filled.Code
import androidx.compose.material.icons.filled.Link
import androidx.compose.material3.*
import androidx.compose.runtime.Composable
import androidx.compose.ui.Alignment
import androidx.compose.ui.Modifier
import androidx.compose.ui.text.AnnotatedString
import presentation.theme.Default
import presentation.theme.HalfDefault
import presentation.theme.QuarterDefault

@Composable
fun ModifierExample(
    title: String,
    body: String,
    codeSnippetImage: String?,
    onShowCodeSnippetClick: (imageResId: String) -> Unit,
    modifier: Modifier = Modifier,
    documentationLink: String? = null,
    exampleContent: @Composable (() -> Unit),
) {

    Column(modifier) {
        Spacer(modifier = Modifier.height(Default))
        Row(Modifier.fillMaxWidth(), verticalAlignment = Alignment.CenterVertically) {
            Text(text = title, style = MaterialTheme.typography.titleMedium)
            Spacer(modifier = Modifier.weight(1f))
            codeSnippetImage?.let {
                IconButton(onClick = { onShowCodeSnippetClick(codeSnippetImage) }) {
                    Icon(imageVector = Icons.Default.Code, contentDescription = "Show the code")
                }
            }
            Spacer(
                modifier = Modifier
                    .width(HalfDefault)
                    .defaultMinSize(HalfDefault)
            )
            documentationLink?.let {
                IconButton(onClick = {
//                    Intent(Intent.ACTION_VIEW, documentationLink?.toUri()).also {
//                        context.startActivity(it)
//                    }
                }) {
                    Icon(
                        imageVector = Icons.Default.Link,
                        contentDescription = "Open documentation link"
                    )
                }
            }
        }
        Spacer(modifier = Modifier.height(HalfDefault))
        Text(
            text = body,
            style = MaterialTheme.typography.bodyMedium
        )
        Spacer(modifier = Modifier.height(HalfDefault))
        exampleContent()
        Spacer(modifier = Modifier.height(QuarterDefault))
        Divider()
    }
}

@Composable
fun ModifierExample(
    title: AnnotatedString,
    body: AnnotatedString,
    codeSnippetImage: String,
    onShowCodeSnippetClick: (imageResId: String) -> Unit,
    documentationLink: String? = null,
    modifier: Modifier = Modifier,
    exampleContent: @Composable (() -> Unit),
) {
    Column(modifier) {
        Spacer(modifier = Modifier.height(Default))
        Row(Modifier.fillMaxWidth(), verticalAlignment = Alignment.CenterVertically) {
            Text(text = title, style = MaterialTheme.typography.titleMedium)
            Spacer(modifier = Modifier.weight(1f))
            IconButton(onClick = { onShowCodeSnippetClick(codeSnippetImage) }) {
                Icon(imageVector = Icons.Default.Code, contentDescription = "Show the code")
            }
            Spacer(
                modifier = Modifier
                    .width(HalfDefault)
                    .defaultMinSize(HalfDefault)
            )
            IconButton(onClick = {
//                Intent(Intent.ACTION_VIEW, documentationLink?.toUri()).also {
//                    context.startActivity(it)
//                }
            }) {
                Icon(
                    imageVector = Icons.Default.Link,
                    contentDescription = "Open documentation link"
                )
            }
        }
        Spacer(modifier = Modifier.height(HalfDefault))
        Text(
            text = body,
            style = MaterialTheme.typography.bodyMedium
        )
        Spacer(modifier = Modifier.height(HalfDefault))
        exampleContent()
        Spacer(modifier = Modifier.height(QuarterDefault))
        Divider()
    }
}

@Composable
fun ModifierExamplePreview() {
//    ModifierExample(
//        title = "Modifier Name",
//        body = "Modifier description",
//        codeSnippetImage = R.drawable.modifier_draw_with_content,
//        onShowCodeSnippetClick = {},
//    ) {
//        Text(text = "Look at this text that contains two rectangles one is drawn before (Behind) and another after (In front of) the content",
//            modifier = Modifier
//                .background(Color.White)
//                .drawWithContent {
//                    drawRect(Color.Yellow, size = size.copy(width = size.width / 2))
//                    drawContent()
//                    drawRect(
//                        Color.Yellow,
//                        alpha = 0.8f,
//                        topLeft = Offset(size.width / 2, 0f),
//                        size = size.copy(width = size.width / 2)
//                    )
//                }
//                .padding(HalfDefault)
//        )
//    }
}
