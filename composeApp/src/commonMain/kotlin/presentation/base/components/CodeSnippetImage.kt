package presentation.base.components

import androidx.compose.foundation.Image
import androidx.compose.foundation.clickable
import androidx.compose.foundation.gestures.animateZoomBy
import androidx.compose.foundation.gestures.detectTapGestures
import androidx.compose.foundation.gestures.rememberTransformableState
import androidx.compose.foundation.gestures.transformable
import androidx.compose.foundation.layout.fillMaxSize
import androidx.compose.foundation.layout.wrapContentHeight
import androidx.compose.material3.Surface
import androidx.compose.runtime.*
import androidx.compose.runtime.saveable.rememberSaveable
import androidx.compose.ui.Modifier
import androidx.compose.ui.geometry.Offset
import androidx.compose.ui.graphics.Color
import androidx.compose.ui.graphics.graphicsLayer
import androidx.compose.ui.input.pointer.pointerInput
import kotlinx.coroutines.launch
import org.jetbrains.compose.resources.ExperimentalResourceApi
import org.jetbrains.compose.resources.painterResource
import kotlin.math.max

@OptIn(ExperimentalResourceApi::class)
@Composable
fun CodeSnippetImage(currentCodeSnippet: String?, onDismiss: () -> Unit) {

    val coroutineScope = rememberCoroutineScope()
    var zoom by rememberSaveable {
        mutableFloatStateOf(1f)
    }
    var offset by remember {
        mutableStateOf(Offset(0f, 0f))
    }

    val transformationState =
        rememberTransformableState(onTransformation = { zoomChange: Float, offsetChange: Offset, _ ->
            zoom = max(zoom * zoomChange, 1f)
            offset += offsetChange
        })

    Surface(
        color = Color.Gray.copy(alpha = 0.5f),
        modifier = Modifier
            .fillMaxSize()
            .clickable(onClick = onDismiss)
            .transformable(
                transformationState,
            )
    ) {
        currentCodeSnippet?.let { imageRes ->
            Image(
                painter = painterResource(imageRes),
                contentDescription = "Code Snippet",
                modifier = Modifier
                    .wrapContentHeight()
                    .graphicsLayer {
                        scaleX = zoom
                        scaleY = zoom
                        translationX = offset.x
                        translationY = offset.y
                    }
                    .pointerInput(Unit) {
                        detectTapGestures(onDoubleTap = {
                            if (zoom > 3f)
                                coroutineScope.launch {
                                    transformationState.animateZoomBy(1 / zoom)
                                }
                            else
                                coroutineScope.launch {
                                    transformationState.animateZoomBy(zoom * 2)
                                }
                        })
                    }
            )
        }
    }
}
